/**
*
* Footer
*
*/

import React from 'react';


import styles from './styles.css';

function Footer() {
  const els = [
    {
      imgName: 'securitycenter',
      label: 'Bloom Credit Security Center',
      href: '#'
    },
    {
      imgName: 'customersupport',
      label: 'Customer Support',
      href: '#'
    },
    {
      imgName: 'experian',
      label: 'Experian Compliant',
      href: '#'
    }
  ]
  return (
    <footer className={styles.footer}>
      <div className={styles.outer}>
        <ul className={styles.inner}>
          {els.map(({imgName, href, label}, idx) => {
            return (
              <li className={styles.liel}>
                <img src={require(`../../../assets/${imgName}.svg`)} alt=""/>
                <a className={styles.txt} href={href}>{label}</a>
              </li>
            )
          })}
        </ul>
      </div>

    </footer>
  );
}

export default Footer;
