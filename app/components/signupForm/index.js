import React from 'react'
import { Field, reduxForm } from 'redux-form/immutable'

import styles from './styles.css'


const SimpleForm = (props) => {
  const { handleSubmit, pristine, reset, submitting } = props
  return (
    <form className={styles.forminput} onSubmit={handleSubmit}>
      <div>
        <Field name="fullName" component="input" type="text" placeholder="Full Name"/>
      </div>
      <div>
        <Field name="email" component="input" type="email" placeholder="Email"/>
      </div>
      <div>
        <Field name="address" component="input" type="text" placeholder="Address"/>
      </div>
      <div>
        <Field name="ssn" component="input" type="password" placeholder="4 last digits of your SSN"/>
      </div>
      <div>
        <Field name="bank" component="input" type="text" placeholder="Link to your Bank Account"/>
      </div>
    </form>
  )
}

export default reduxForm({
  form: 'registration'  // a unique identifier for this form
})(SimpleForm)
