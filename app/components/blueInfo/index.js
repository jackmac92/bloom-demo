import React from 'react';
import IMG from '../reacTags/IMG'
import styles from './styles.css'

export default ({heading, body, img, isTopPic}) => {
  let bottomPic = null, topPic = null;
  return (
    <div className={styles.card}>
      {isTopPic && <img className={styles.imageR} src={img} alt="Fun pic"/>}
      <h5 className={styles.heading}>{heading}</h5>
      <p className={styles.body}>{body}</p>
      {!isTopPic && <img className={styles.imageL} src={img} alt="Fun pic"/>}
    </div>
  );

}
