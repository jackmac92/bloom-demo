import React from 'react';
import styles from './styles.css'

export default ({name, features, cons, img}) => {

  return (
    <div className={styles.product}>
      <h2 className={styles.productName}>{name}</h2>
      <img className={styles.productImg} src={require(`../../../assets/${img}.svg`)} alt={`${name} card pic`}/>
      <ul className={styles.featureList}>
        {features.map((f, idx) => (
            <li className={styles.feature} key={`f${idx}`}>
              <img src={require('./feature.svg')} alt=""/>
              {f}
            </li>
          )
        )}
        {cons.map((c, idx) => (
          <li className={styles.feature} key={`c${idx}`}>
            <img src={require('./con.svg')} alt=""/>
            <span>{c}</span>
          </li>
          )
        )}
      </ul>
    </div>
  );

}
