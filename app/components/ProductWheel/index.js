import React from 'react';
import Slider from 'react-slick'
import Button from '../reacTags/Button'
import IMG from '../reacTags/IMG'
import Product from '../Product'
import styles from './styles.css'

export default ({products}) => {
  const sliderSettings = {
    adaptiveHeight: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    infinite: true
  };


  return (
    <div className={styles.wrapper}>
      <h2 className={styles.head}>Product Recomendations</h2>
      <span className={styles.subhead}>Please select and preview a card to apply</span>
      <Slider {...sliderSettings}>
        {products.map((p, idx) => <div key={p.id}><Product {...p}/></div>)}
      </Slider>
      <Button>Access this Product</Button>

    </div>
  );

}
