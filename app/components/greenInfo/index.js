import React from 'react';
import IMG from '../reacTags/IMG'
import styles from './styles.css'
export default ({heading, body, img}) => {

  return (
    <div className={styles.card}>
      <IMG
        src={img}
        alt="Fun pic"
      />
      <h5 className={styles.heading}>{heading}</h5>
      <p className={styles.body}>{body}</p>
    </div>
  );

}
