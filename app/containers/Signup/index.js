/*
 *
 * Signup
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import selectSignup from './selectors';
import Button from '../../components/reacTags/Button'
import SimpleForm from '../../components/signupForm'
import Footer from '../../components/Footer'
import styles from './styles.css';

export class Signup extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className={styles.signup}>
        <div className={styles.chevron}></div>
        <h1 className={styles.head}>Welcome to Bloom Credit!</h1>
        <h4 className={styles.subHead}>Our goal is to help you gain access to the financial products and services you desire by helping you <a className={styles.subHeadLink} href="#">Improve Your Credit Score</a></h4>
        <div className={styles.first}>First, we need to know the following information about you:</div>
        <div className={styles.formWrap}>
          <SimpleForm />
        </div>
        <Link className={styles.why} href="#">Why do you need this info?</Link>
        <div className={styles.begin}>Ready To Get Started?</div>
        <Button handleRoute={() => browserHistory.push("/user")}>Click here to begin</Button>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = selectSignup();

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Signup);
