/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */

export default {
  history: {
    dashimg: "userdash1",
    head: "Yet to Establish Credit",
    desc: "You've yet to establish credit. Simply, this means that credit bureaus don't know how to evaluate you yet.",
    detail: [
      "Building credit can be tricky. If you don’t have a credit history, it’s hard to get a loan, a credit card or even an apartment. To have a FICO score, for example, you need at least one account that’s been open six months or longer, and you need at least one creditor reporting your activity to the credit bureaus in the last six months. (A VantageScore, from FICO’s biggest competitor, can be generated more quickly.)",
      "Several tools can help you establish a credit history: secured credit cards, a credit-builder loan, a co-signed credit card or loan, or authorized user status on another person’s credit card."
    ],
    suggestion: {
      head: "Secured Credit Card",
      body: [
        "Whether you have bad credit or you’re building a credit score from scratch, a secured credit card can speed you on your way to better terms.",
        "Unlike prepaid debit cards, secured cards can help you establish or improve your credit history, provided that the issuer reports your account activity to the credit bureaus, TransUnion, Experian and Equifax. With secured cards, you’re required to make a refundable deposit as collateral, similar to putting a security deposit down on an apartment. That money protects the card issuer in case you don’t pay your bill."
      ]
    },
    products: [
      {
        name: "Capital One Secured Card",
        id: 0,
        features: [
          "Qualify with low credit",
          "24.99%APR",
          "No Foreign transaction fees",
          "No annual fee"
        ],
        cons: [
          "No rewards"
        ],
        img: "visaproduct"
      },
      {
        name: "American Bank Installment Loan",
        id: 1,
        features: [
          "Qualify with low credit",
          "16.7% APR",
          "No Prepayment Penalties",
          "1 year repayment term"
        ],
        cons: [],
        img: "visaproduct"
      }
    ]
  },
  balance: {
    id: 'app.components.HomePage.header',
    defaultMessage: 'This is HomePage components !',
  },
}
