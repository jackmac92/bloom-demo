/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import info from './messages.js'
import ProductWheel from '../../components/ProductWheel'
import Footer from '../../components/Footer'
import Button from '../../components/reacTags/Button'
import styles from './styles.css'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

export default class Profile extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      finLitOpen: false
    };
    this.openFinLit = this.openFinLit.bind(this);
  }
  openFinLit() {
    this.setState({
      finLitOpen: !this.state.finLitOpen
    })
  }
 render() {
  const pageInfo = info.history
  return (
    <div>
      <div className={styles.dash}> <img className={styles.dashImg} src={require(`./${pageInfo.dashimg}.svg`)} alt=""/></div>
      <br/>
      <hr />
      <br/>

      <h3 className={styles.header}>{`Credit Diagnosis - ${pageInfo.head}`}</h3>
      <span className={styles.txt}>{pageInfo.desc}</span>
      <Button onClick={this.openFinLit}>What does this mean?</Button>
      <ReactCSSTransitionGroup
        transitionName="finlit"
        transitionAppear={true}
        transitionAppearTimeout={5000}
      >
        { (this.state.finLitOpen) ?
            <div key={"visible"} className={styles.finlit}>
              <div>
                {pageInfo.detail.map((txt, idx) => <p className={styles.txt} key={idx}>{txt}</p>)}
              </div>
              <div className={styles.solutionBanner}>Suggested Solution</div>
              <h3 className={styles.solutionHead}>{pageInfo.suggestion.head}</h3>
              {pageInfo.suggestion.body.map((txt, idx) => <p className={styles.txt} key={idx}>{txt}</p>)}
              <div className={styles.contact}>
                <img src={require('../../../assets/customersupport.svg')} alt=""/>
                <div className={styles.lnk}><a href="mailto:support@emissary.io">Contact someone to have this solution explained</a></div>
              </div>
            </div>
        :
            null
        }
      </ReactCSSTransitionGroup>
      <hr />
      <ProductWheel
        products={pageInfo.products}
      />
      <Footer />
    </div>
  );
 }
}
