import { createSelector } from 'reselect';

/**
 * Direct selector to the header state domain
 */
const selectHeaderDomain = () => (state) => state.get('header');

const selectUser = () => createSelector(
  selectHeaderDomain(),
  (state) => state.get('user')
)

/**
 * Other specific selectors
 */


/**
 * Default selector used by Header
 */

const selectHeader = () => createSelector(
  selectHeaderDomain(),
  (substate) => substate.toJS()
);

export default selectHeader;
export {
  selectHeaderDomain,
  selectUser
};
