/*
 *
 * Header
 *
 */

import React from 'react';
import { Link } from 'react-router'
import { connect } from 'react-redux';
import selectHeader from './selectors';
import IMG from '../../components/reacTags/IMG'
import styles from './styles.css'
import { push } from 'redux-router'

export class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props)
  }

  render() {
    const { user, dispatch } = this.props;
    const headerLinks = [
      {
        label: "Home",
        path: "/"
      },
      {
        label: "FAQ",
        path: "/faq"
      },
      {
        label: "Credit Scores Explained",
        path: ""
      }
    ]
    return (

      <header className={styles.header}>
        <Link to="/" ><IMG onClick={() => { dispatch(push("/"))}} className={styles.logo} src={require("./logo.png")} alt="Logo" /></Link>
        <div className={styles.headerRight}>
          <div className={styles.headerAvatar}>
            <span className={styles.username}>{user.name}</span>
          </div>
          <ul className={styles.headerNav}>
            {
              headerLinks.map((el, idx) => <li key={idx} className={styles.headerLi}><Link className={styles.headerLink} to={el.path}>{el.label}</Link></li> )
            }
          </ul>
        </div>
      </header>

    );

  }
}

const mapStateToProps = selectHeader();

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
