import { fromJS } from 'immutable';

const initialState = fromJS({
  user: {
    name: "John Doe",
  }
})

export default (state = initialState, action) => {
  switch(action.type) {
    default:
      return state;
  }
}
