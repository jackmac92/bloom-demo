/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux'
import messages from './messages';
import styles from './styles.css'
import selectHomePage from './selectors'
import { browserHistory } from 'react-router'

import BlueInfo from '../../components/blueInfo'
import GreenInfo from '../../components/greenInfo'
import Button from '../../components/reacTags/Button'

class HomePage extends React.Component { // eslint-disable-line react/prefer-stateless-function

 render() {
  const { user } = this.props;
  return (
    <div>
      <h1 className={styles.headr}>Gain access to the best possible financing</h1>
      <h4 className={styles.subheader}>Receive Real Advice to​ Improve your Credit Score. Get alerted when the bank is ready for you.</h4>
      <Button handleRoute={() => {browserHistory.push("/signup")}}>Start improving your credit score</Button>

      <div className={styles.subwelcome}>
        <div className={styles.swelcome}>Welcome</div>
        <div className={styles.sname}>{user.name}</div>
      </div>

      <br />
      <hr />
      <div className={styles.blueOuter}>
      <div className={styles.blue}>
        <BlueInfo
          img={require('../../../assets/moneyripple.svg')}
          heading="What is Bloom Credit?"
          body="Bloom Credit is a Credit Monitoring service that alerts you when you become eligible for the financial products you want most. We provide you real, actionable, advice that informs you about how to improve your credit score, and reconnect you with your institution of choice. Best of all, it costs you nothing!"
          isTopPic={false}
        />

        <BlueInfo
          img={require('../../../assets/fingerclipboard.svg')}
          heading="How Does it Work?"
          body="Bloom Credit uses technology to analyze your credit report. We identify the factors that are most prevalent in hurting your credit score and provide you a personalized report as to the actions you can take to improve your credit."
          isTopPic={true}
        />
      </div>
      </div>
      <div className={styles.greenOuter}>
      <div className={styles.green}>
        <h3 className={styles.msg}>What Will You Get?</h3>

        <GreenInfo
          heading="Continued Credit Guidance"
          body="Bloom Credit will continuously analyze your credit report to give you ways you can be improving"
          img={require('../../../assets/green0.svg')}
        />
        <GreenInfo
          heading="Financial Access"
          body="Bloom Credit works with financial institutions to reintroduce you to them, right at the point you obtain eligibility"
          img={require('../../../assets/green1.svg')}
        />
        <GreenInfo
          heading="Better Rates"
          body="By improving your credit score, you’ll be able to access better rates to all lending products across the board"
          img={require('../../../assets/green2.svg')}
        />
        <GreenInfo
          heading="Financial Literacy"
          body="Understand the why behind your credit report to ensure good financial habits"
          img={require('../../../assets/green3.svg')}
        />
      </div>
      </div>

      <div className={styles.footMsg}>
        <hr />
        <br />
        <h3 className={styles.msg}>Are you Ready to Begin improving your Credit Score?</h3>
        <Button handleRoute={() => {browserHistory.push("/signup")}}>Begin Here</Button>
      </div>

    </div>

  );
 }
}

HomePage.propTypes = {
  user: React.PropTypes.object
}

export function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const mapStateToProps = selectHomePage()

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
